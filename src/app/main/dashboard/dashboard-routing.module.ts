import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CirclesComponent } from './circles/circles.component';
import { DashboardComponent } from './dashboard.component';
import { MembersComponent } from './members/members.component';


const routes: Routes = [
  {path:"",component:DashboardComponent,
  children:[
    {path:"members",component:MembersComponent},
    {path:"circles",component:CirclesComponent},
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
