import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { MembersComponent } from './members/members.component';
import { CirclesComponent } from './circles/circles.component';


@NgModule({
  declarations: [DashboardComponent, MembersComponent, CirclesComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
