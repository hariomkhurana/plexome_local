import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainComponent } from "./main.component";
import { AuthGuardMain } from "../auth/auth-guard.service";

const routes: Routes = [
  {
    path: "main",
    component: MainComponent,
    canActivate: [AuthGuardMain],
    children: [
      {
        path: "dashboard",
        loadChildren: () =>
          import("../main/dashboard/dashboard.module").then((m) => m.DashboardModule),
      },
      // {
      //   path: "manager",
      //   loadChildren: () =>
      //     import("./manager/manager-management.module").then((m) => m.ManagerManagementModule),
      //     data: { state: 'manager' } 
      // },
      // {
      //   path: "employee",
      //   loadChildren: () =>
      //     import("./employees/employee.module").then((m) => m.EmployeeModule),
      //     data: { state: 'employee' } 
      // },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}
