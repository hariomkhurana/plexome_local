
import { Component, HostListener, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthServiceLocal } from '../../auth.service';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
 
  loginForm: FormGroup;
  emailPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/;
  message:string = "";
  formSubmitted :boolean = false;
  isLoading: boolean = false; 
  public screenWidth: any;
  public screenHeight: any;
  constructor(private fb: FormBuilder,
    private authServices: AuthServiceLocal,
    private router: Router,
    private _snackBar: MatSnackBar
    ) { 
  
    }

  ngOnInit() {
    this.initialiseForms();

  }
  initialiseForms() {
    this.loginForm = this.fb.group({
      email: ["", [Validators.required,Validators.pattern(this.emailPattern)]],
    });
  }

  submitForm() {
    this.formSubmitted = true;
    if (this.loginForm.valid) {
      let req = {
        email: this.loginForm.controls["email"].value.toLowerCase(),
        "type": "FP"
      };
      this.isLoading = true;
      this.authServices.forgotPassword(req).subscribe(
        (res) => {
          if ((res["message"] = "Success")) {
            this.isLoading = false;
            this.loginForm.reset()
            this.router.navigate(["signin"])
            return this._snackBar.open(res.data.message, "", {
                 duration: 3000,
                   horizontalPosition: "right",
                   verticalPosition: "top",
                  panelClass: ["success"],
                });
               
           
          } else {
          }
        },
        (err) => {
          this.isLoading = false;
        }
      );


    }


  }
  naviateLogin(data) {
     if(data['isPlan'] == false) {
      this.router.navigate(["signup"])
      localStorage.setItem("SignUp",'2')
    }
    else if (data['isProfile'] == false) {
      this.router.navigate(["signup"])
      localStorage.setItem("SignUp",'4')
    }
    else {
      this.router.navigate(["main/dashboard"])
    }
  }
  readLocalStorageValue(key: string): string {
    return localStorage.getItem(key);
  }


}
