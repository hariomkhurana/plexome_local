import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../../shared/shared.module';
import { AuthServiceLocal } from '../auth.service';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
const router: Routes = [
  {path:"",component:LoginComponent,
children:[
  { path: '', redirectTo: 'signin', pathMatch: 'full' },
  {path:"signin",component:SigninComponent, data: { state: 'signin' } },
  {path:"signup",component:SignupComponent, data: { state: 'signun' } },
  {path:"forgot-password",component:ForgotPasswordComponent, data: { state: 'forgot' } },
],
},
];

@NgModule({
  exports:[],
  imports: [CommonModule,SharedModule, RouterModule.forChild(router), FormsModule, ReactiveFormsModule, HttpClientModule],
  declarations: [LoginComponent,  SigninComponent, ForgotPasswordComponent, SignupComponent],
  providers:[AuthServiceLocal]
})
export class LoginModule { }
