import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CommonService } from "../../../shared/common.service";
import { AuthServiceLocal } from "../../auth.service";
import { Router } from "@angular/router";
import { HttpResponse } from "@angular/common/http";
import { StreamService } from "../../../shared/stream.service";
import { environment } from "../../../../environments/environment";
import { MatSnackBar } from "@angular/material";
import Swal from "sweetalert2";
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  loginForm: FormGroup;
  resetForm:FormGroup;
  formSubmitted:boolean = false;
  isLoading:boolean = false;
  show: boolean = false;
  isLogin:boolean = true;
  emailPattern = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/;
  message: string = "";
  type:string = "0";
  constructor(    private fb: FormBuilder,
    private authServices: AuthServiceLocal,
    private commonService: CommonService,
    private streamService: StreamService,
    private router: Router,
    private _snackBar: MatSnackBar,) { }

  ngOnInit() {
    this.initialiseForms();
  }
  // login() {
  //   this.router.navigate(["main/admin/dashboard"]);
  //  
  // }

  initialiseForms() {
    this.loginForm = this.fb.group({
      email: ["", [Validators.required, Validators.pattern(this.emailPattern)]],
      password: ["", [Validators.required, Validators.minLength(6)]],
      type: ["0", [Validators.required]]
    });
    this.resetForm = this.fb.group({
      email: ["", [Validators.required, Validators.pattern(this.emailPattern)]],
    });
  }

  // validatEmail() {
  //   this.formSubmitted = true;
  //   if (this.resetForm.valid) {
  //     let req = {
  //       email: this.resetForm.controls["email"].value.toLowerCase(),
  //     };
  //     this.isLoading = true;
  //     this.authServices.verifyEmail(req).subscribe(
  //       (res) => {
  //         if ((res["message"] = "Success")) {
  //           this.resetSubmit();
  //         } else {
  //         }
  //       },
  //       (err) => {
  //         this._snackBar.open("Email Verified", "", {
  //           duration: 3000,
  //             horizontalPosition: "right",
  //             verticalPosition: "top",
  //            panelClass: ["success"],
  //          });
  //       }
  //     );


  //   }


  // }

  login() {
    this.router.navigate(["main/dashboard"]);
        // this.formSubmitted = true;
          // let req = {
          //   email: this.loginForm.controls["email"].value,
          //   password: this.loginForm.controls["password"].value
          // };
          // this.isLoading = true;
          // this.authServices.otherLogin(req).subscribe (
          //   (res: HttpResponse<any>) => {
          //     this.isLoading = false;
          //     if ((res["message"] = "Success")) {
          //       console.log("response", res);
          //       this.commonService.setStorage("token",res.headers.get('Authorization'));

          //       if(res.body.data["accessToken"]) {
          //         this.commonService.setStorage(
          //           "token",
          //           res.body.data["accessToken"]
          //         );
          //       }

          //       this.sendRouteVal(0);
          //       this.commonService.setStorage(
          //         "token",
          //         res.body.data["accessToken"]
          //       );
          //     } else {
          //     }
          //   },
          //   err => {
          //     // Swal.fire({
          //     //   icon: "error",
          //     //   title: err.error.data.message
          //     // });
          //    this._snackBar.open(err.error.data.message, "", {
          //       duration: 3000,
          //         horizontalPosition: "right",
          //         verticalPosition: "top",
          //        panelClass: ["success"],
          //      });
          //     this.isLoading = false;
          //   }
          // );
  }
  // navigateForgot(value) {
  //   if (value == 'true') {
  //     this.isLogin = true;
  //     this.loginForm.reset();  this.initialiseForms();

  //   } else {
  //     this.isLogin = false;
  //     this.loginForm.reset();
    
  //   }
  // }

 


  // sendRouteVal(index) {
  //     console.log("SEND LOGIN")
  //     this.streamService.streamMessage(index)
  //     // this.messageEvent.emit(index);
  //   }
  //   password() {
  //     this.show = !this.show;
  //   }
  }
