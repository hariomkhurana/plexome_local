import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { CommonService } from "../shared/common.service";

@Injectable({providedIn: "root"})
export class AuthGuardMain implements CanActivate {
  constructor(private router: Router, private commonService: CommonService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.commonService.isAuthenticated()) {
      let userData = this.commonService.getUser();
      // console.log('inside out guarard',route.data,userData)
      console.log(route.data,userData,'inside meeee')
      if (route.data.role && route.data.role.indexOf(userData.role) == -1) {
        console.log('inside In guarard')
        if (userData['role'] =='admin') {
          this.router.navigate(["/main/admin/dashboard"])
        }
        if (userData['role'] =='employee') {
          this.router.navigate(["/main/employee/dashboard"])
        }
        if (userData['role'] =='manager') {
          this.router.navigate(["/main/manager/dashboard"])
        }
       
        return false;
      } else {
        console.log('inside OUT guarard')
        return true;
      }
    }else {
      this.router.navigate(["/signin"])
      return false;
    }
  }

}
