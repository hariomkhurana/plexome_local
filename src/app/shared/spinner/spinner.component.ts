import { Component, OnInit, Output,EventEmitter, Input } from '@angular/core';
@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {

  @Input('type') type: string = 'fullScreen';
  constructor() { }

  ngOnInit(): void {
  }

}
