import { Injectable } from "@angular/core";
import { CommonService } from "../common.service";


export interface Menu {
  state: string;
  name: string;
  type: string;
  iconUrl: string;
  iconUrlActive:string
}

const MENUITEMS = [
  { state: 'dashboard', name: 'Dashboard', iconUrl:"assets/images/Dashboard_icon.svg", type: 'link', iconUrlActive:'assets/images/Dashboard_icon_white.svg'},
  
];
// const MENUMANAGER = [
// { state: 'manager/dashboard', name: 'Dashboard', iconUrl:"assets/images/Dashboard_icon.svg", type: 'link', iconUrlActive:'assets/images/Dashboard_icon_white.svg'},
// { state: 'manager/list-resource', name: 'Resources', iconUrl:"assets/images/Resources_icon.svg", type: 'link', iconUrlActive:'assets/images/Resources_icon_white.svg'},
// { state: 'manager/list-assessment',  name: 'Assessment',iconUrl:"assets/images/Assessment_icon.svg", type: 'link', iconUrlActive:'assets/images/Assessment_icon_white.svg' },
// { state: 'manager/list-employee', name: 'Employees', iconUrl:"assets/images/Dashboard_icon.svg", type: 'link', iconUrlActive:'assets/images/Dashboard_icon_white.svg'},
// { state: 'manager/list-assessment',  name: 'Assessment Assigned', iconUrl:"assets/images/Employees_icon.svg", type: 'Nolink', iconUrlActive:'assets/images/Assessment_icon_white.svg'},

// ];
// const MENUEMPLOYEE = [
// { state: 'employee/dashboard', name: 'Dashboard', iconUrl:"assets/images/Dashboard_icon.svg", type: 'link', iconUrlActive:'assets/images/Dashboard_icon_white.svg'},
// { state: 'employee/list-resource', name: 'Resources', iconUrl:"assets/images/Resources_icon.svg", type: 'link', iconUrlActive:'assets/images/Resources_icon_white.svg'},
// { state: 'employee/list-assessment', name: 'Assessment ', iconUrl:"assets/images/Assessment_icon.svg", type: 'link', iconUrlActive:'assets/images/Assessment_icon_white.svg'},
//  ];

@Injectable()
export class MenuItems {
  userData
  constructor(private commonService:CommonService) {
    this.userData = this.commonService.getUser();
    this.getMenuitem();
    
  }
  getMenuitem(): Menu[] {
    return MENUITEMS; 
  }

}
