
import { MediaMatcher } from '@angular/cdk/layout';
import { NavigationStart, Router } from '@angular/router';
import {
  ChangeDetectorRef,
  Component,
  NgZone,
  OnDestroy,
  ViewChild,
  HostListener,
  Directive,
  AfterViewInit,
  OnInit
} from '@angular/core';
import { MenuItems } from '../../shared/menu-items/menu-items';
import { CommonService } from '../../shared/common.service';
import {
  transition,
  trigger,
  query,
  style,
  animate,
  group,
  animateChild,
  state
} from '@angular/animations';
import { MatSnackBar } from '@angular/material';
import { AuthServiceLocal } from '../../auth/auth.service';
import { StreamService } from '../../shared/stream.service';

declare var $: any;
/** @title Responsive sidenav */
@Component({
  selector: 'app-full-layout',
  templateUrl: 'full.component.html',
  styleUrls: ['full.component.scss'],
  animations: [
    trigger('routerTransition', [
      transition('* <=> *', [    
        query(':enter, :leave', style({ position: 'fixed', opacity: 0 }),{ optional: true }),
        group([ 
          query(':enter', [
            style({ opacity:0 }),
            animate('600ms ease-in-out', style({ opacity:0 })),

          ],{ optional: true }),
          query(':leave', [
            style({ opacity:0.5 }),
            animate('600ms ease-in-out', style({ opacity:0 }))],{ optional: true }),
        ])
      ])
    ])
   ]
})
export class FullComponent implements OnDestroy, AfterViewInit,OnInit {
  mobileQuery: MediaQueryList;
  isMenuOpen = true;
  contentMargin: number ;
  activeIndex:any;
  message: number;
  filterMessage:any;
  public screenWidth: any;
  public screenHeight: any;
  isMobile: boolean = false;
  detail:any;
  Name:string;
  routeName:string;
  profilePic:string;
  headerTitle = [];
  filterVal:string
  userData:any;
  userRole:any
  showScroll : boolean = true;
  isLoading:boolean = false;
  isPodcast:boolean = false;
  NoScroll =["Search","My Matches","My Favorites"];
  currentScreenWidth:any;
  currentNavition:any;
  adminFilterAccess:[2,3,4,5]
  private _mobileQueryListener: () => void;

 

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems,
    public common :CommonService,
    public router :Router,
    private streamService: StreamService

  ) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
       
        // if (event.url.startsWith('/main/')) {
        //   this.message = parseInt(localStorage.getItem('activeIndex'));
        //   this.getHeader();
        // }
      }
    });
     this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    console.log('mobileQuery',this.mobileQuery.matches)

  }

  ngOnInit() {

    // console.log(this.activeIndex)
    // this.userData = this.common.getUser();

    // this.getHeader();
    // this.streamService.getSubscription().subscribe(message => {
    //   console.log(message);
    //   this.message = message.data;
    //   this.getHeader();
    //   console.log(this.message, "MESSAGE");
    // });

    // this.streamService.getFilterSubscription().subscribe(data => {
    //   console.log(data);
    //   this.filterMessage = data.data;
    //   console.log(this.message, "subscribed to filter header");
    // });
  
    $(".rotate").click(function () {
      $(this).toggleClass("down");
  })
  this.currentScreenWidth = screen.width ;
  if( this.currentScreenWidth <=  780) {
    this.isMobile = true;
  }
  console.log(this.currentScreenWidth,"CURRENTSCREEN")
  }
  navName:string;
  getHeader() {

    this.headerTitle = this.currentNavition;
    console.log(this.headerTitle,"TITLENAME")
    this.navName  = this.headerTitle[this.message].name;
    console.log(this.navName,"FINAL")

  };
  // checkFilterAccessValidation() {
  //   if(this.userData.role = 'admin') {
  //     for(let i =0; i < this.adminFilterAccess.length; i++) {
       
  //     }

  //   }
  
  // }
changePasswordNav() {
  // this.checkFilterAccessValidation()
  console.log("called")
if(this.userData.role =='manager') {
  localStorage.setItem("activeIndex",'4')
  this.getHeader()
  console.log("MANAGER")

  this.router.navigate(["/main/manager/change-password"]) 
} 
if(this.userData.role =='employee') {
  localStorage.setItem("activeIndex",'3')
  this.getHeader()
  console.log("Emp")
  this.router.navigate(["/main/employee/change-password"]) 
}
}

  logoutUser() { 
    this.common.logOut();
}
filterFunction() {
  console.log(this.filterVal,"entered")
  this.streamService.streamFilterMessage(this.filterVal);
}
enterFilterVal() {
  console.log(this.filterVal,"entered")
  this.streamService.streamFilterMessage(this.filterVal);
}

  // navigateSroll() {
  //   this.showScroll = this.NoScroll.indexOf(this.headerTitle) == -1;
  // }


  WindowResize(e) {
    // setTimeout(() => { window.dispatchEvent(new Event('resize')); }, 260);
    }

    ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  readLocalStorageValue(key: string): number {
    return parseInt(localStorage.getItem(key));
  }
  logOut() {
    this.common.logOut();
  }
  ngAfterViewInit() {}

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = window.innerWidth;
    this.screenHeight = window.innerHeight;
    console.log(this.screenWidth,'sss')
    if( this.screenWidth <= 780) {
      console.log("ISMOBILE TRUE")
      this.isMobile = true;
      
    }
    else {
      console.log("ISMOBILE FALSEs")
      this.isMobile =false;
    }

     console.log('isMobile',this.isMobile)
  }
}
